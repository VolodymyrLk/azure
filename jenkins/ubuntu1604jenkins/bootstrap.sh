#!/usr/bin/env bash

echo ""
echo "------------------------------"
echo "installing apache"
apt-get update -y && apt-get upgrade -y
apt-get install -y apache2
echo "apache installation finished"

echo ""
echo "------------------------------"
echo "opening the firewall"
ufw allow 80
ufw status
ufw allow OpenSSH
ufw --force enable
echo "finished"

echo ""
echo "------------------------------"
echo "update and upgrade"
apt-get update -y && apt-get upgrade -y
echo "update and upgrade finished"

echo ""
echo "------------------------------"
echo "openjdk-8-jdk installation"
apt-get install openjdk-8-jdk -y
java -version
echo "openjdk-8-jdk finished"

echo ""
echo "------------------------------"
echo "jenkins installation"
wget -q -O - https://pkg.jenkins.io/debian/jenkins.io.key | sudo apt-key add -
sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
apt-get update -y
apt-get install -y jenkins
systemctl start jenkins
systemctl status jenkins
echo "jenkins finished"

echo ""
echo "------------------------------"
echo "opening the firewall"
ufw allow 8080
ufw status
ufw allow OpenSSH
ufw --force enable
echo "finished"

echo ""
echo "------------------------------"
echo "ansible installing"
sudo apt-get install software-properties-common -y
sudo apt-add-repository ppa:ansible/ansible -y
sudo apt-get update -y
sudo apt-get install ansible -y